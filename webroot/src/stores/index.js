import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import rootReducer from '../reducers';
import { name, version } from '../../package.json';

let enhancer;

if (process.env.NODE_ENV === 'development') {
  const composeEnhancers = composeWithDevTools({
    name: `${name}@${version}`,
  });
  enhancer = composeEnhancers(applyMiddleware(thunk));
} else {
  enhancer = applyMiddleware(thunk);
}

export const configureStore = () => createStore(rootReducer, enhancer);
