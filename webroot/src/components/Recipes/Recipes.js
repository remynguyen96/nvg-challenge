import React, { useState, memo, useEffect } from 'react';
import PropTypes from 'prop-types';

import { RecipeItem } from '../RecipeItem';
import './Recipes.css';

function Recipes({ recipeList, onRemove, onSearch }) {
  const [recipes, setRecipes] = useState([]);
  const [keyword, setKeyword] = useState('');

  useEffect(() => {
    setRecipes(recipeList);
  }, []);

  const onSearchRecipes = (event) => {
    const { value } = event.target;
    const newRecipes = onSearch(value.toLowerCase());

    setKeyword(value);
    setRecipes(newRecipes);
  };
  const onRemoveRecipe = (id) => onRemove(id).then(newRecipes => setRecipes(newRecipes));

  return (
    <>
      <div className="Recipes-search" data-testid="recipesSearch">
        <input
          type="text"
          className="Recipes-input"
          placeholder="Search title recipe..."
          value={keyword}
          onChange={onSearchRecipes}
        />
      </div>
      {recipes.map((item) => <RecipeItem key={item.id} {...item} onRemoveRecipe={onRemoveRecipe} />)}
    </>
  );
}

Recipes.propTypes = {
  recipeList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      created: PropTypes.string.isRequired,
      modified: PropTypes.string.isRequired,
      ingredients: PropTypes.array.isRequired,
    }),
  ).isRequired,
  onRemove: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired,
};

export default memo(Recipes);
