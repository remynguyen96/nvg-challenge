import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Recipes from '../Recipes';

const props = {
  recipeList: [
    {
      'title': 'Roasted bake',
      'description': 'Heat the oil in a medium pan and fry the onions, garlic, bay leaf and chilli for 3 mins. Add the cumin, paprika and cayenne, season, then cook for a further 3 mins. Add the pepper and cook for 5 mins until it starts to soften.\n\nAdd the chopped tomatoes and stock, lower the heat and simmer for 20-25 mins until you have a thick ragu, then stir in the finely chopped coriander.\n\nHeat oven to 180C/160C fan/gas 4. Lay each flatbread on a baking tray. Put half the vegetable mix in the centre of each and spread it out a bit, keeping at least an inch spare all the way around as a border. Make a small well in the centre and crack an egg on top of each flatbread. Crumble over the feta and put in the oven for 12-15 mins until the egg is set and the cheese melted slightly. Scatter over the remaining coriander and serve.',
      'ingredients': [
        {
          'name': 'olive oil for frying',
          'amount': 1,
          'unit': 'teaspoon',
        },
      ],
      'id': '1',
      'created': '2018-11-09T16:35:03.922Z',
      'modified': '2018-11-09T16:35:03.922Z',
    },
    {
      'title': 'Crock Pot Roast',
      'description': 'Place beef roast in crock pot.\n\nMix the dried mixes together in a bowl and sprinkle over the roast.\n\nPour the water around the roast.\n\nCook on low for 7-9 hours.',
      'ingredients': [
        {
          'name': 'water',
          'amount': 0.5,
          'unit': 'cup',
        },
      ],
      'id': '2',
      'created': '2018-11-12T12:06:36.944Z',
      'modified': '2018-11-12T12:06:36.944Z',
    },
  ],
  onRemove: jest.fn(),
  onSearch: jest.fn(),
};

describe('Recipes Component', () => {
  afterEach(cleanup);

  it('should take a snapshot with normal props', () => {
    const { asFragment } = render(<Recipes {...props} />);

    expect(asFragment(<Recipes {...props} />)).toMatchSnapshot();
  });

  it('should renders correctly', () => {
    const { getByTestId } = render(<Recipes {...props} />);

    expect(getByTestId('recipesSearch')).toHaveClass('Recipes-search');
  });
});