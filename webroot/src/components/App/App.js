import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchRecipeList, deleteRecipeItem, searchRecipes } from '../../actions';
import { Recipes } from '../Recipes';
import './App.css';

const TXT_ANNOUNCE_ERROR = 'Please refresh the browser and try again.';

function App({ recipeList, errorMsg, getRecipeList, removeRecipe, searchTitleRecipes }) {
  useEffect(() => {
    getRecipeList();
  }, []);

  const isEmptyRecipes = recipeList.length === 0;

  if (errorMsg) return <h2>{TXT_ANNOUNCE_ERROR}</h2>;

  return isEmptyRecipes ? <span data-testid="appEmpty" className="App-empty">Empty Recipes!</span> : (
    <section data-testid="app" className="App">
      <Recipes recipeList={recipeList} onRemove={removeRecipe} onSearch={searchTitleRecipes} />
    </section>
  );
}

App.propTypes = {
  recipeList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      created: PropTypes.string.isRequired,
      modified: PropTypes.string.isRequired,
      ingredients: PropTypes.array.isRequired,
    }),
  ).isRequired,
  getRecipeList: PropTypes.func.isRequired,
  removeRecipe: PropTypes.func.isRequired,
  searchTitleRecipes: PropTypes.func.isRequired,
  errorMsg: PropTypes.string,
};

App.defaultProps = {
  errorMsg: '',
};

const mapStateToProps = ({ recipes: { recipeList, errorMsg } }) => ({
  recipeList,
  errorMsg,
});

const mapDispatchToProps = (dispatch) => ({
  getRecipeList() {
    dispatch(fetchRecipeList());
  },
  removeRecipe(id) {
    return dispatch(deleteRecipeItem(id));
  },
  searchTitleRecipes(title) {
    return dispatch(searchRecipes(title));
  },
});
const enhance = connect(mapStateToProps, mapDispatchToProps)(App);

export default memo(enhance);
