import React from 'react';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import { render, cleanup, act } from '@testing-library/react';

import * as actions from '../../../actions';
import App from '../App';

const recipeList = [
  {
    'title': 'Roasted bake',
    'description': 'Heat the oil in a medium pan and fry the onions, garlic, bay leaf and chilli for 3 mins. Add the cumin, paprika and cayenne, season, then cook for a further 3 mins. Add the pepper and cook for 5 mins until it starts to soften.\n\nAdd the chopped tomatoes and stock, lower the heat and simmer for 20-25 mins until you have a thick ragu, then stir in the finely chopped coriander.\n\nHeat oven to 180C/160C fan/gas 4. Lay each flatbread on a baking tray. Put half the vegetable mix in the centre of each and spread it out a bit, keeping at least an inch spare all the way around as a border. Make a small well in the centre and crack an egg on top of each flatbread. Crumble over the feta and put in the oven for 12-15 mins until the egg is set and the cheese melted slightly. Scatter over the remaining coriander and serve.',
    'ingredients': [
      {
        'name': 'olive oil for frying',
        'amount': 1,
        'unit': 'teaspoon',
      },
      {
        'name': 'vegetable stock',
        'amount': 200,
        'unit': 'ml',
      },
    ],
    'id': '1',
    'created': '2018-11-09T16:35:03.922Z',
    'modified': '2018-11-09T16:35:03.922Z',
  },
  {
    'title': 'Crock Pot Roast',
    'description': 'Place beef roast in crock pot.\n\nMix the dried mixes together in a bowl and sprinkle over the roast.\n\nPour the water around the roast.\n\nCook on low for 7-9 hours.',
    'ingredients': [
      {
        'name': 'water',
        'amount': 0.5,
        'unit': 'cup',
      },
    ],
    'id': '2',
    'created': '2018-11-12T12:06:36.944Z',
    'modified': '2018-11-12T12:06:36.944Z',
  },
  {
    'title': 'Roasted Asparagus',
    'description': 'Preheat oven to 425°F.\n\nCut off the woody bottom part of the asparagus spears and discard.\n\nWith a vegetable peeler, peel off the skin on the bottom 2-3 inches of the spears (this keeps the asparagus from being all.",string.", and if you eat asparagus you know what I mean by that).\n\nPlace asparagus on foil-lined baking sheet and drizzle with olive oil.\n\nSprinkle with salt.\n\nWith your hands, roll the asparagus around until they are evenly coated with oil and salt.\n\nRoast for 10-15 minutes, depending on the thickness of your stalks and how tender you like them.\n\nThey should be tender when pierced with the tip of a knife.\n\nThe tips of the spears will get very brown but watch them to prevent burning.\n\nThey are great plain, but sometimes I serve them with a light vinaigrette if we need something acidic to balance out our meal.',
    'ingredients': [
      {
        'name': 'kosher salt',
        'amount': 1,
        'unit': 'teaspoon',
      },
    ],
    'id': '3',
    'created': '2018-11-12T12:06:48.957Z',
    'modified': '2018-11-12T12:06:48.957Z',
  },
];

const mockStore = configureStore([thunk]);

describe('App Component', () => {
  let initialState, store, fetchRecipes;

  beforeEach(() => {
    initialState = { recipes: { recipeList: [], errorMsg: '' } };
    store = mockStore(initialState);
    // mock recipes data from fetch on useEffect.
    fetchRecipes = Promise.resolve(recipeList);
    actions.fetchRecipeList = jest.fn(() => () => fetchRecipes);
  });

  afterEach(cleanup);

  it('should take a snapshot', async () => {
    const { asFragment, rerender } = render(<Provider store={store}><App /></Provider>);

    await act(() => fetchRecipes); // Remove warning did not wrap in act(...)

    store = mockStore({ recipes: { recipeList, errorMsg: '' } }); // update props from store reducer.

    rerender(<Provider store={store}><App /></Provider>); // rerender component to update newest from store.

    expect(asFragment(<Provider store={store}><App /></Provider>)).toMatchSnapshot();
  });

  it('should take a snapshot when receive an error', async () => {
    initialState = { recipes: { recipeList: [], errorMsg: 'Test Error Message' } };
    store = mockStore(initialState);

    const { asFragment } = render(<Provider store={store}><App /></Provider>);

    await act(() => fetchRecipes); // Remove warning did not wrap in act(...)

    expect(asFragment(<Provider store={store}><App /></Provider>)).toMatchSnapshot();
  });

  it('should renders correctly without any stores', async () => {
    const { getByTestId } = render(<Provider store={store}><App /></Provider>);

    expect(getByTestId('appEmpty')).toHaveClass('App-empty');
  });

  it('should renders correctly', async () => {
    const { getByTestId, rerender } = render(<Provider store={store}><App /></Provider>);

    await act(() => fetchRecipes); // Remove warning did not wrap in act(...)

    store = mockStore({ recipes: { recipeList, errorMsg: '' } }); // update props from store reducer.

    rerender(<Provider store={store}><App /></Provider>); // rerender component to update newest from store.

    expect(getByTestId('app')).toHaveClass('App');
  });
});


