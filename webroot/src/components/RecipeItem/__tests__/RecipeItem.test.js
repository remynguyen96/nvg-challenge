import React from 'react';
import { render, cleanup } from '@testing-library/react';

import RecipeItem from '../RecipeItem';

const props = {
  title: 'Crock Pot Roast',
  description: 'Place beef roast in crock pot.\n\nMix the dried mixes together in a bowl and sprinkle over the roast.\n\nPour the water around the roast.\n\nCook on low for 7-9 hours.',
  ingredients: [
    {
      'name': 'water',
      'amount': 0.5,
      'unit': 'cup',
    },
  ],
  id: '2',
  created: '2018-11-12T12:06:36.944Z',
  modified: '2018-11-12T12:06:36.944Z',
  onRemoveRecipe: jest.fn(),
};

describe('RecipeItem Component', () => {
  afterEach(cleanup);

  it('should take a snapshot with normal props', () => {
    const { asFragment } = render(<RecipeItem {...props} />);

    expect(asFragment(<RecipeItem {...props} />)).toMatchSnapshot();
  });

  it('should renders correctly', () => {
    const { getByTestId } = render(<RecipeItem {...props} />);

    expect(getByTestId('recipeItem')).toHaveClass('RecipeItem');
  });
});