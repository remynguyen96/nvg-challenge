import React, { memo } from 'react';
import PropTypes from 'prop-types';

import './RecipeItem.css';

function Recipes({ id, title, description, created, modified, ingredients, onRemoveRecipe }) {
  const onDelete = () => onRemoveRecipe(id);

  return (
    <div data-testid="recipeItem" className="RecipeItem">
      <span className="RecipeItem-txt">ID: {id}</span>
      <span className="RecipeItem-txt">TITLE: {title}</span>
      <span className="RecipeItem-txt">DESCRIPTION: {description}</span>
      <span className="RecipeItem-txt">MODIFIED: {modified}</span>
      <span className="RecipeItem-txt">CREATED: {created}</span>
      <span className="RecipeItem-txt">INGREDIENTS: {JSON.stringify(ingredients)}</span>
      <button type="button" className="RecipeItem-btn" onClick={onDelete}>Delete</button>
    </div>
  );
}

Recipes.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  created: PropTypes.string.isRequired,
  modified: PropTypes.string.isRequired,
  ingredients: PropTypes.array.isRequired,
  onRemoveRecipe: PropTypes.func.isRequired,
};

export default Recipes;
