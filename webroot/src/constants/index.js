export const API_RECIPES = 'http://localhost:8081/recipes';

export const FINISH_GET_RECIPES = 'FINISH_GET_RECIPES';
export const ERROR_GET_RECIPES = 'ERROR_GET_RECIPES';

export const DELETE_RECIPE_ITEM = 'DELETE_RECIPE_ITEM';
export const ERROR_DELETE_RECIPE = 'ERROR_DELETE_RECIPE';
