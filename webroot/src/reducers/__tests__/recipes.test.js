import { recipes as reducer } from '../recipes';
import { FINISH_GET_RECIPES, DELETE_RECIPE_ITEM, ERROR_DELETE_RECIPE, ERROR_GET_RECIPES } from '../../constants';

describe('recipes reducer', () => {
  it('action FINISH_GET_RECIPES should return correct state', () => {
    const recipes = ['recipe1', 'recipe2', 'recipe3'];
    const action = { type: FINISH_GET_RECIPES, payload: recipes };
    const result = reducer({}, action);

    expect(result.recipeList).toEqual(recipes);
  });

  it('action ERROR_GET_RECIPES should return correct state', () => {
    const errorMsg = 'failed';
    const action = { type: ERROR_GET_RECIPES, payload: errorMsg };
    const result = reducer({}, action);

    expect(result.errorMsg).toEqual(errorMsg);
  });

  it('action DELETE_RECIPE_ITEM should return correct state', () => {
    const recipes = ['recipe1', 'recipe2'];
    const action = { type: DELETE_RECIPE_ITEM, payload: recipes };
    const result = reducer({}, action);

    expect(result.recipeList).toEqual(recipes);
  });


  it('action ERROR_DELETE_RECIPE should return correct state', () => {
    const errorMsg = 'failedDelete';
    const action = { type: ERROR_DELETE_RECIPE, payload: errorMsg };
    const result = reducer({}, action);

    expect(result.errorMsg).toEqual(errorMsg);
  });


});