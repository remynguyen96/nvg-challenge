import rootReducer from '../index';

describe('rootReducer', () => {
  it('should return a state', () => {
    expect(typeof rootReducer).toBe('function');
    expect(typeof rootReducer({}, {})).toBe('object');
  });
});