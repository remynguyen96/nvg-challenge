import {
  FINISH_GET_RECIPES,
  ERROR_GET_RECIPES,
  DELETE_RECIPE_ITEM,
  ERROR_DELETE_RECIPE,
} from '../constants';

export const initialRecipes = {
  recipeList: [],
  errorMsg: '',
};

export const recipes = (state = initialRecipes, action = {}) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_RECIPE_ITEM:
    case FINISH_GET_RECIPES:
      return { ...state, recipeList: [...payload] };
    case ERROR_DELETE_RECIPE:
    case ERROR_GET_RECIPES:
      return { ...state, errorMsg: payload };
    default:
      return state;
  }
};