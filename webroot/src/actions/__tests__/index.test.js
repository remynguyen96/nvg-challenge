import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import {
  finishFetchingAction,
  deleteRecipeItemAction,
  errorFetchingAction,
  errorDeleteAction,
  fetchRecipeList,
  deleteRecipeItem,
  searchRecipes,
} from '../index';

const mockStore = configureStore([thunk]);
const recipeList = [
  {
    'title': 'Roasted bake',
    'description': 'Heat the oil in a medium pan and fry the onions, garlic, bay leaf and chilli for 3 mins. Add the cumin, paprika and cayenne, season, then cook for a further 3 mins. Add the pepper and cook for 5 mins until it starts to soften.\n\nAdd the chopped tomatoes and stock, lower the heat and simmer for 20-25 mins until you have a thick ragu, then stir in the finely chopped coriander.\n\nHeat oven to 180C/160C fan/gas 4. Lay each flatbread on a baking tray. Put half the vegetable mix in the centre of each and spread it out a bit, keeping at least an inch spare all the way around as a border. Make a small well in the centre and crack an egg on top of each flatbread. Crumble over the feta and put in the oven for 12-15 mins until the egg is set and the cheese melted slightly. Scatter over the remaining coriander and serve.',
    'ingredients': [
      {
        'name': 'olive oil for frying',
        'amount': 1,
        'unit': 'teaspoon',
      },
      {
        'name': 'vegetable stock',
        'amount': 200,
        'unit': 'ml',
      },
    ],
    'id': '1',
    'created': '2018-11-09T16:35:03.922Z',
    'modified': '2018-11-09T16:35:03.922Z',
  },
  {
    'title': 'Crock Pot Roast',
    'description': 'Place beef roast in crock pot.\n\nMix the dried mixes together in a bowl and sprinkle over the roast.\n\nPour the water around the roast.\n\nCook on low for 7-9 hours.',
    'ingredients': [
      {
        'name': 'water',
        'amount': 0.5,
        'unit': 'cup',
      },
    ],
    'id': '2',
    'created': '2018-11-12T12:06:36.944Z',
    'modified': '2018-11-12T12:06:36.944Z',
  },
  {
    'title': 'Roasted Asparagus',
    'description': 'Preheat oven to 425°F.\n\nCut off the woody bottom part of the asparagus spears and discard.\n\nWith a vegetable peeler, peel off the skin on the bottom 2-3 inches of the spears (this keeps the asparagus from being all.",string.", and if you eat asparagus you know what I mean by that).\n\nPlace asparagus on foil-lined baking sheet and drizzle with olive oil.\n\nSprinkle with salt.\n\nWith your hands, roll the asparagus around until they are evenly coated with oil and salt.\n\nRoast for 10-15 minutes, depending on the thickness of your stalks and how tender you like them.\n\nThey should be tender when pierced with the tip of a knife.\n\nThe tips of the spears will get very brown but watch them to prevent burning.\n\nThey are great plain, but sometimes I serve them with a light vinaigrette if we need something acidic to balance out our meal.',
    'ingredients': [
      {
        'name': 'kosher salt',
        'amount': 1,
        'unit': 'teaspoon',
      },
    ],
    'id': '3',
    'created': '2018-11-12T12:06:48.957Z',
    'modified': '2018-11-12T12:06:48.957Z',
  },
];

describe.only('recipes actions', () => {
  let store;

  beforeEach(() => {
    store = mockStore({ recipes: { recipeList: [] } });
    fetch.resetMocks();
  });

  describe('fetchRecipeList function', () => {
    it('should execute fetch data', async () => {
      fetch.mockResponse(JSON.stringify({ recipes: recipeList }));

      await store.dispatch(fetchRecipeList());

      const actions = store.getActions();
      const expectResult = [...recipeList];
      expect(actions[0]).toEqual(finishFetchingAction(expectResult));
    });

    it('should catch the error when fetch data', async () => {
      const errorMsg = 'fake error message';
      fetch.mockReject(new Error(errorMsg));

      await store.dispatch(fetchRecipeList());

      const actions = store.getActions();
      expect(actions[0]).toEqual(errorFetchingAction(errorMsg));
    });
  });

  describe('deleteRecipeItem function', () => {
    let id;
    beforeEach(() => {
      id = 1;
    });

    it('should execute delete data', async () => {
      fetch.mockResponse(JSON.stringify({}));

      await store.dispatch(deleteRecipeItem(id));

      const actions = store.getActions();
      expect(actions[0]).toEqual(deleteRecipeItemAction([]));
    });

    it('should catch the error when delete data', async () => {
      const errorMsg = 'fake error message';
      fetch.mockReject(new Error(errorMsg));

      await store.dispatch(deleteRecipeItem(id));

      const actions = store.getActions();
      expect(actions[0]).toEqual(errorDeleteAction(errorMsg));
    });
  });

  describe('searchRecipes function', () => {

    beforeEach(() => {
      store = mockStore({ recipes: { recipeList } });
    });

    it('should search and return correct data', () => {
      const title = 'ROASTED';
      const dataSearch = store.dispatch(searchRecipes(title.toLowerCase()));

      const expectResult = [recipeList[0], recipeList[2]];

      expect(dataSearch).toEqual(expectResult);
    });

    it('should search and return empty data', () => {
      const title = 'TEST';
      const dataSearch = store.dispatch(searchRecipes(title.toLowerCase()));

      expect(dataSearch).toEqual([]);
    });
  });
});