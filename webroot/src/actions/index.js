import {
  API_RECIPES,
  FINISH_GET_RECIPES,
  ERROR_GET_RECIPES,
  DELETE_RECIPE_ITEM,
  ERROR_DELETE_RECIPE,
} from '../constants';

export function finishFetchingAction(payload) {
  return { type: FINISH_GET_RECIPES, payload };
}

export function errorFetchingAction(payload) {
  return { type: ERROR_GET_RECIPES, payload };
}

export function deleteRecipeItemAction(payload) {
  return { type: DELETE_RECIPE_ITEM, payload };
}

export function errorDeleteAction(payload) {
  return { type: ERROR_DELETE_RECIPE, payload };
}

/**
 * Fetch API to get list recipes and save on redux storage.
 * @returns {Promise<Array>}
 */
export function fetchRecipeList() {
  return async (dispatch) => {
    try {
      const fetchRecipesList = await fetch(API_RECIPES);
      const { recipes } = await fetchRecipesList.json();

      dispatch(finishFetchingAction(recipes));

      return recipes;
    } catch (err) {
      dispatch(errorFetchingAction(err.message));
    }
  };
}

/**
 * Delete Recipe API and update list recipe on redux storage.
 * @returns {Promise<Array>}
 */
export function deleteRecipeItem(id) {
  return async (dispatch, states) => {
    try {
      const { recipeList } = states().recipes;
      const deleteRecipeItem = await fetch(`${API_RECIPES}/${id}`, {
        method: 'DELETE',
        body: { id },
      });
      await deleteRecipeItem.json();

      const newRecipeList = recipeList.filter((recipe) => recipe.id !== id);

      dispatch(deleteRecipeItemAction(newRecipeList));

      return newRecipeList;
    } catch (err) {
      dispatch(errorDeleteAction(err.message));
    }
  };
}

/**
 * Search Recipes.
 * @returns {Array}
 */
export function searchRecipes(title) {
  return (_, states) => {
    const { recipeList } = states().recipes;
    const filterRecipes = recipeList.filter(recipe => {
      return recipe.title.toLowerCase().includes(title);
    });

    return filterRecipes;
  };
}