import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import './index.css';
import { configureStore } from './stores';
import { App } from './components/App';

ReactDOM.render(
  <React.StrictMode>
      <Provider store={configureStore()}>
          <App />
      </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

